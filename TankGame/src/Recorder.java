import java.io.*;
import java.util.Vector;

/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Recorder {
    private static int killCount;//击毁敌方坦克数属性
    private static String path = "src\\myRecord.txt";//保存路径
    private static BufferedWriter bw = null;//写入路径文件
    private static BufferedReader br = null;//读取路径文件
    private static Vector<Enemy> enemies = null;//敌人坦克集合
    private static Vector<Node> nodes = new Vector<>();//添加node集合

    public static void setEnemies(Vector<Enemy> enemies) {//获取敌人坦克集合
        Recorder.enemies = enemies;
    }

    //保存数据方法
    public static void keepRecord() {
        if (enemies.size() != 0) {//如果敌人数量不为零则创建文件并保存数据，否则删除文件
            try {//捕获异常
                bw = new BufferedWriter(new FileWriter(path));//添加写入文件的路径
                bw.write(killCount + "\r\n");//保存击毁敌人坦克数，并换行（注：数据类型为字符型）
                for (int i = 0; i < enemies.size(); i++) { //遍历敌人
                    Enemy enemy = enemies.get(i);
                    if (enemy.isLive) {//判断是否存活
                        String info = enemy.getX() + " " + enemy.getY() + " " + enemy.getDirect();//敌人信息以空格隔开
                        bw.write(info + "\r\n");//写入敌人信息
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (bw != null) {
                    try {//捕获异常
                        bw.close();//br不为空则关闭（注：关闭才能写入数据）
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {//没有坦克则获胜，不需要记录游戏信息
            File file = new File(path);
            file.delete();//删除游戏信息
        }
    }

    //获取上局游戏信息方法
    public static Vector<Node> getLastInfo() {//记录信息，调用时返回坦克信息集合
        try {
            br = new BufferedReader(new FileReader(path));//添加读取文件路径
            killCount = Integer.parseInt(br.readLine());//读取一行并转为Integer赋值给killCount
            String line;
            while ((line = br.readLine()) != null) {//当读取不为空是继续
                String[] s = line.split(" ");//以空格为标识符String数组
                //创建Node坦克数据并初始化
                Node node = new Node(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2]));
                nodes.add(node);//添加进入nodes坦克信息集合
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();//不为空时关闭
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return nodes;//返回nodes信息
    }


    public static int getKillCount() {//获取击毁敌人坦克信息
        return killCount;
    }

    public static void addKillCount() {//击毁坦克时调用
        killCount++;
    }

    public static String getPath() {//获取路径信息
        return path;
    }

}