/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Tank {
    private int x;//x坐标
    private int y;//有坐标
    private int direct;//方向
    private int speed = 1;//速度
    boolean isLive = true;//坦克存活
    public void moveUp() {
        if (y >= 3) {
            y -= speed;
        }
    }

    public void moveDown() {
        if (y <= 707) {
            y += speed;
        }
    }

    public void moveRight() {
        if (x <= 957) {
            x += speed;
        }
    }

    public void moveLeft() {
        if (x >= 3 ) {
            x -= speed;
        }
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public Tank(int x, int y, int direct) {
        this.x = x;
        this.y = y;
        this.direct = direct;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDirect() {
        return direct;
    }

    public void setDirect(int direct) {
        this.direct = direct;
    }
}