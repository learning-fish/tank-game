import java.util.Vector;

/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Hero extends Tank {
    Vector<Shot> shots = new Vector<>();//创建子弹集合，后续限制子弹个数
    private Shot shot = null;//子弹

    public Hero(int x, int y, int direct) {//构造器
        super(x, y, direct);
    }

    public void shotEnemy() {//根据方向设置子弹发射位置
        if (shots.size() == 3) {//如果子弹数量等于3就不添加子弹
            return;
        }
        switch (getDirect()) {
            case 0:
                shot = new Shot(getX() + 20, getY(), 0);
                break;
            case 1:
                shot = new Shot(getX() + 40, getY() + 20, 1);
                break;
            case 2:
                shot = new Shot(getX() + 20, getY() + 40, 2);
                break;
            case 3:
                shot = new Shot(getX(), getY() + 20, 3);
                break;
        }
        new Thread(shot).start();//启动子弹线程
        shots.add(shot);//添加进子弹集合
    }
}