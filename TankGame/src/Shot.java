
/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Shot implements Runnable {
    private int x;//子弹x坐标
    private int y;//子弹y坐标
    private int speed = 8;//子弹速度
    private int direct;//子弹方向
    private boolean isLive = true;//子弹是否存活

    public Shot(int x, int y, int direct) {//Shot的构造器
        this.x = x;
        this.y = y;
        this.direct = direct;
    }


    @Override
    public void run() {//根据移动方向设置子弹移动方向
        while (true) {
            try {
                Thread.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            switch (direct) {
                case 0:
                    y -= speed;
                    break;
                case 1:
                    x += speed;
                    break;
                case 2:
                    y += speed;
                    break;
                case 3:
                    x -= speed;
                    break;
            }

            if (!(x >= 0 && x <= 1000 && y >= 0 && y <= 750 && isLive)) {//子弹超界退出循环
                isLive = false;
                break;
            }
        }


    }

    public boolean getIsLive() {
        return isLive;
    }

    public void setIsLive(boolean live) {

        isLive = live;
    }


    public int getX() {

        return x;
    }

    public int getY() {
        return y;
    }
}
