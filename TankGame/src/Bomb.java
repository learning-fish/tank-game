/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Bomb {
    private int x;
    private int y;
    private int live = 12;

    public Bomb(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void lifeDown() {
        if (live > 0) {
            live--;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getLive() {
        return live;
    }
}
