import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class MyPanel extends JPanel implements KeyListener, Runnable {
    Vector<Hero> heroes = new Vector<>();//创建我方坦克集合
    int enemyNum = 10;//创建敌方坦克数量
    Vector<Enemy> enemies = new Vector<>();//创建敌方坦克集合
    Vector<Bomb> bombs = new Vector<>();//创建爆炸集合
    Image image1 = null;//新建3个图片属性
    Image image2 = null;
    Image image3 = null;
    Vector<Node> nodes = new Vector<>();//创建信息集合

    public MyPanel(String key) {//有参构造器

        //初始化我方坦克
        Hero hero = new Hero(550, 500, 0);
        hero.setSpeed(5);//设置我方移动速度
        heroes.add(hero);//添加我方坦克集合

        //初始化敌方坦克前，判断是否继续游戏
        switch (key) {
            case "Y":
                nodes = Recorder.getLastInfo();//调用方法获取上局游戏的敌人信息
                for (int i = 0; i < nodes.size(); i++) {//遍历获取每个敌人信息
                    Node node = nodes.get(i);
                    Enemy enemy = new Enemy(node.getX(), node.getY(), node.getDirect());//赋值上局敌人位置
                    enemy.setSpeed(2);//设置敌人移动速度
                    enemies.add(enemy);//添加至敌人坦克集合
                    new Thread(enemy).start();//启动敌人坦克线程
                    enemy.setEnemies(enemies);//传入enemies判断是否碰撞
                    Recorder.setEnemies(enemies);//传入enemies记录游戏信息
                }
                break;
            case "N":
                for (int i = 0; i < enemyNum; i++) {
                    Enemy enemy = new Enemy(100 * (i + 0), 0, 2);
                    enemy.setSpeed(2);//设置敌人移动速度
                    enemies.add(enemy);//添加至敌人坦克集合
                    new Thread(enemy).start();//启动敌人坦克线程
                    enemy.setEnemies(enemies);//传入enemies判断是否碰撞
                    Recorder.setEnemies(enemies);//传入enemies记录游戏信息
                }
                break;
        }

        //初始化爆炸图片属性
        image1 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_1.gif"));
        image2 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_2.gif"));
        image3 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_3.gif"));

        //启动音频
        new AePlayWave("src\\111.wav").start();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.fill3DRect(0, 0, 1000, 750, false);//设置背景大小

        //我方坦克
        for (int i = 0; i < heroes.size(); i++) {//如果坦克数量为零则不画出
            Hero hero = heroes.get(i);
            drawTank(hero.getX(), hero.getY(), hero.getDirect(), 0, g);//画出我方坦克
            for (int j = 0; j < hero.shots.size(); j++) {//当子弹不为空时画出子弹
                Shot shot = hero.shots.get(j);
                if (shot.getIsLive()) {//判断子弹是否超界，未超界画出
                    g.fill3DRect(shot.getX() - 1, shot.getY() - 1, 3, 3, false);
                } else {
                    hero.shots.remove(shot);//超界移除
                }
            }
        }

        //敌方坦克
        for (int i = 0; i < enemies.size(); i++) {//遍历画出敌方坦克
            Enemy enemy = enemies.get(i);
            if (enemy.isLive) {//判断存活才画出坦克
                drawTank(enemy.getX(), enemy.getY(), enemy.getDirect(), 1, g);
            }
            for (int j = 0; j < enemy.shots.size(); j++) {//当子弹不为空时画出子弹
                Shot shot = enemy.shots.get(j);
                if (shot.getIsLive()) {//判断子弹是否超界，未超界画出
                    g.fill3DRect(shot.getX() - 1, shot.getY() - 1, 3, 3, false);
                } else {
                    enemy.shots.remove(shot);//超界移除
                }
            }
        }

        showInfo(g);//调用方法显示游戏信息

        //显示爆炸信息
        for (int i = 0; i < bombs.size(); i++) {//有爆炸对象画出
            Bomb bomb = bombs.get(i);
            int x = bomb.getX();
            int y = bomb.getY();
            int live = bomb.getLive();
            if (live > 6) {
                g.drawImage(image1, x, y, 40, 40, this);//bomb_1.gif
            } else if (live > 3) {
                g.drawImage(image2, x, y, 40, 40, this);//bomb_2.gif
            } else {
                g.drawImage(image3, x, y, 40, 40, this);//bomb_3.gif
            }
            bomb.lifeDown();
            if (live == 0) {//完成后移除
                bombs.remove(bomb);
            }
        }
    }


    public void showInfo(Graphics g) {//显示游戏信息
        g.setColor(Color.BLACK);
        Font font = new Font("宋体", Font.BOLD, 25);
        g.setFont(font);
        g.drawString("累计击毁敌方坦克", 1050, 30);
        g.drawString(Recorder.getKillCount() + "", 1110, 80);//直接从Recorder中获取信息
        drawTank(1050, 50, 0, 1, g);

        if (enemies.size() == 0) {//胜利信息
            g.setColor(Color.red);
            Font font1 = new Font("宋体", Font.BOLD, 80);
            g.setFont(font1);
            g.drawString("YOU WIN", 350, 350);
        }

        if (heroes.size() == 0) {//失败信息
            g.setColor(Color.red);
            Font font2 = new Font("宋体", Font.BOLD, 80);
            g.setFont(font2);
            g.drawString("Game Over", 350, 350);
        }
    }


    /**
     * @param x      坦克的横坐标
     * @param y      坦克的纵坐标
     * @param g      画笔
     * @param direct 坦克的方向
     * @param type   坦克的颜色
     */
    public void drawTank(int x, int y, int direct, int type, Graphics g) {//设置坦克模板

        switch (type) {//坦克颜色区分敌我坦克
            case 0:
                g.setColor(Color.CYAN);
                break;
            case 1:
                g.setColor(Color.YELLOW);
                break;
        }

        switch (direct) {//坦克方向
            case 0://上
                g.fill3DRect(x, y, 10, 40, false);//坦克左轮
                g.fill3DRect(x + 30, y, 10, 40, false);//坦克右轮
                g.fill3DRect(x + 10, y + 5, 20, 30, false);//坦克身子
                g.fillOval(x + 10, y + 10, 20, 20);//坦克盖子
                g.fill3DRect(x + 18, y - 2, 4, 4, false);//
                g.drawLine(x + 20, y + 20, x + 20, y);//坦克炮膛
                for (int i = 2; i < 40; i += 4) {
                    g.drawLine(x + 1, y + i, x + 9, y + i);
                    g.drawLine(x + 31, y + i, x + 39, y + i);
                }
                break;
            case 1://右
                g.fill3DRect(x, y, 40, 10, false);
                g.fill3DRect(x, y + 30, 40, 10, false);
                g.fill3DRect(x + 5, y + 10, 30, 20, false);
                g.fillOval(x + 10, y + 10, 20, 20);
                g.fill3DRect(x + 38, y + 18, 4, 4, false);
                g.drawLine(x + 20, y + 20, x + 40, y + 20);
                for (int i = 2; i < 40; i += 4) {
                    g.drawLine(x + i, y + 1, x + i, y + 9);
                    g.drawLine(x + i, y + 31, x + i, y + 39);
                }
                break;
            case 2://下
                g.fill3DRect(x, y, 10, 40, false);
                g.fill3DRect(x + 30, y, 10, 40, false);
                g.fill3DRect(x + 10, y + 5, 20, 30, false);
                g.fillOval(x + 10, y + 10, 20, 20);
                g.fill3DRect(x + 18, y + 38, 4, 4, false);
                g.drawLine(x + 20, y + 20, x + 20, y + 40);
                for (int i = 2; i < 40; i += 4) {
                    g.drawLine(x + 1, y + i, x + 9, y + i);
                    g.drawLine(x + 31, y + i, x + 39, y + i);
                }
                break;
            case 3://左
                g.fill3DRect(x, y, 40, 10, false);
                g.fill3DRect(x, y + 30, 40, 10, false);
                g.fill3DRect(x + 5, y + 10, 30, 20, false);
                g.fillOval(x + 10, y + 10, 20, 20);//坦克盖子
                g.fill3DRect(x - 2, y + 18, 4, 4, false);
                g.drawLine(x + 20, y + 20, x, y + 20);
                for (int i = 2; i < 40; i += 4) {
                    g.drawLine(x + i, y + 1, x + i, y + 9);
                    g.drawLine(x + i, y + 31, x + i, y + 39);

                }
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        for (int i = 0; i < heroes.size(); i++) {
            Hero hero = heroes.get(i);
            if (e.getKeyCode() == KeyEvent.VK_W) {//上
                hero.moveUp();
                hero.setDirect(0);
            } else if (e.getKeyCode() == KeyEvent.VK_D) {//右
                hero.setDirect(1);
                hero.moveRight();
            } else if (e.getKeyCode() == KeyEvent.VK_S) {//下
                hero.setDirect(2);
                hero.moveDown();
            } else if (e.getKeyCode() == KeyEvent.VK_A) {//左
                hero.setDirect(3);
                hero.moveLeft();
            }
            if (e.getKeyCode() == KeyEvent.VK_J) {//按J发射子弹
                hero.shotEnemy();
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void hit(Shot shot, Tank tank) {//hit方法,判断是否击中坦克
        int x = tank.getX();
        int y = tank.getY();
        if (shot.getX() > x && shot.getX() < x + 40 && shot.getY() > y && shot.getY() < y + 40) {
            tank.isLive = false;//击中坦克，坦克不存活
            shot.setIsLive(false);//击中坦克，子弹不存活
            if (tank instanceof Enemy) {//判断是否为敌方坦克
                enemies.remove(tank);//是敌方tank就从集合中移除
                Recorder.addKillCount();//调用方法记录击毁坦克数
            }
            if (tank instanceof Hero) {//是我方坦克移除
                heroes.remove(tank);
            }
            Bomb bomb = new Bomb(x, y);//新建bomb对象
            bombs.add(bomb);//加入bombs集合中
        }
    }

    @Override
    public void run() {
        while (true) {

            this.repaint();//刷新画板
            try {
                Thread.sleep(15);//每15ms刷新一次
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < enemies.size(); i++) {//遍历敌人坦克
                Enemy enemy = enemies.get(i);//得到敌人

                for (int j = 0; j < enemy.shots.size(); j++) {//遍历敌方子弹
                    Shot shot = enemy.shots.get(j);//得到敌方子弹
                    for (int k = 0; k < heroes.size(); k++) {
                        Hero hero = heroes.get(k);
                        hit(shot, hero);//调用方法判断
                    }
                }
                for (int k = 0; k < heroes.size(); k++) {//遍历我方坦克
                    Hero hero = heroes.get(k);//得到我方坦克
                    for (int j = 0; j < hero.shots.size(); j++) {//遍历我方子弹
                        Shot shot = hero.shots.get(j);//得到我方子弹
                        hit(shot, enemy);//调用方法判断
                    }
                }
            }
        }
    }
}
