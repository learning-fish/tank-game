import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Scanner;

/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class TankGame extends JFrame {
    MyPanel mp = null;
    Scanner scanner = new Scanner(System.in);

    public TankGame() {//重写构造器

        String key;
        File file = new File(Recorder.getPath());
        if(!file.exists()){
            key = "N";
        }else {
            System.out.println("是否继续上局游戏(Y/N):");
            while (true) {
                key = scanner.next().toUpperCase();
                if (key.compareTo("Y") == 0 || key.compareTo("N") == 0) {
                    break;
                }
                System.out.println("输入有误,请重新输入");
            }
        }

        mp = new MyPanel(key);//初始化属性
        new Thread(mp).start();//启动Mypanel的线程
        this.setSize(1300, 800);//设置界面大小
        this.add(mp);//添加mp
        this.setVisible(true);//设置可见
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//设置关闭窗口结束程序
        this.addKeyListener(mp);//监听键盘输入
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Recorder.keepRecord();
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        TankGame tankGame = new TankGame();//启动游戏入口
    }
}
