/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Node {
    private int x;//x坐标
    private int y;//y坐标
    private int direct;//方向

    public Node(int x, int y, int direct) {//构造器传参
        this.x = x;
        this.y = y;
        this.direct = direct;
    }

    public int getX() {
        return x;
    }


    public int getY() {
        return y;
    }


    public int getDirect() {
        return direct;
    }

}