import java.util.Vector;

/**
 * @author 谢阳
 * @version 1.8.0_131
 */
public class Enemy extends Tank implements Runnable {
    Shot shot = null;//子弹属性
    Vector<Shot> shots = new Vector<>();//子弹集合
    Vector<Enemy> enemies = new Vector<>();//坦克集合

    public void setEnemies(Vector<Enemy> enemies) {
        this.enemies = enemies;
    }

    public Enemy(int x, int y, int direct) {
        super(x, y, direct);
    }

    @Override
    public void run() {//重写run方法
        while (true) {
            if (shots.size() < 2) {//设置敌方子弹个数为2
                switch (getDirect()) {
                    case 0:
                        shot = new Shot(getX() + 20, getY(), 0);
                        break;
                    case 1:
                        shot = new Shot(getX() + 60, getY() + 20, 1);
                        break;
                    case 2:
                        shot = new Shot(getX() + 20, getY() + 60, 2);
                        break;
                    case 3:
                        shot = new Shot(getX(), getY() + 20, 3);
                        break;
                }
                new Thread(shot).start();//启动子弹线程
                shots.add(shot);//添加进子弹集合
            }
            int random = (int) (Math.random() * 60);//获取随机移动距离
            switch (getDirect()) {//获取方向
                case 0://上
                    for (int i = 0; i < random; i++) {
                        if (!isTouch())//判断是否碰撞，未碰撞运动
                            moveUp();//向上移动random步
                        try {
                            Thread.sleep(50);//休眠50ms
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 1://右
                    for (int i = 0; i < random; i++) {
                        if (!isTouch())
                            moveRight();//向右移动random步
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 2://下
                    for (int i = 0; i < random; i++) {
                        if (!isTouch())
                            moveDown();//向下移动random步
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 3://左
                    for (int i = 0; i < random; i++) {
                        if (!isTouch())
                            moveLeft();//向左移动random步
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
            setDirect((int) (Math.random() * 4));//设置随机方向
        }
    }
    public boolean isTouch() {
        switch (this.getDirect()) {
            case 0:
                for (int i = 0; i < enemies.size(); i++) {
                    Enemy enemy = enemies.get(i);
                    if (this != enemy) {
                        if (this.getX() >= enemy.getX() &&
                                this.getX() <= enemy.getX() + 40 &&
                                this.getY() >= enemy.getY() &&
                                this.getY() <= enemy.getY() + 40) {
                            return true;
                        }
                        if (this.getX() + 40 >= enemy.getX() &&
                                this.getX() + 40 <= enemy.getX() + 40 &&
                                this.getY() >= enemy.getY() &&
                                this.getY() <= enemy.getY() + 40) {
                            return true;
                        }
                    }
                }
                break;
            case 1:
                for (int i = 0; i < enemies.size(); i++) {
                    Enemy enemy = enemies.get(i);
                    if (this != enemy) {
                        if (this.getX() + 40 >= enemy.getX() &&
                                this.getX() + 40 <= enemy.getX() + 40 &&
                                this.getY() >= enemy.getY() &&
                                this.getY() <= enemy.getY() + 40) {
                            return true;
                        }
                        if (this.getX() + 40 >= enemy.getX() &&
                                this.getX() + 40 <= enemy.getX() + 40 &&
                                this.getY() + 40 >= enemy.getY() &&
                                this.getY() + 40 <= enemy.getY() + 40) {
                            return true;
                        }
                    }
                }
                break;
            case 2:
                for (int i = 0; i < enemies.size(); i++) {
                    Enemy enemy = enemies.get(i);
                    if (this != enemy) {
                        if (this.getX() >= enemy.getX() &&
                                this.getX() <= enemy.getX() + 40 &&
                                this.getY() + 40 >= enemy.getY() &&
                                this.getY() + 40 <= enemy.getY() + 40) {
                            return true;
                        }
                        if (this.getX() + 40 >= enemy.getX() &&
                                this.getX() + 40 <= enemy.getX() + 40 &&
                                this.getY() + 40 >= enemy.getY() &&
                                this.getY() + 40 <= enemy.getY() + 40) {
                            return true;
                        }
                    }
                }
                break;
            case 3:
                for (int i = 0; i < enemies.size(); i++) {
                    Enemy enemy = enemies.get(i);
                    if (this != enemy) {
                        if (this.getX() >= enemy.getX() &&
                                this.getX() <= enemy.getX() + 40 &&
                                this.getY() >= enemy.getY() &&
                                this.getY() <= enemy.getY() + 40) {
                            return true;
                        }
                        if (this.getX() >= enemy.getX() &&
                                this.getX() <= enemy.getX() + 40 &&
                                this.getY() + 40 >= enemy.getY() &&
                                this.getY() + 40 <= enemy.getY() + 40) {
                            return true;
                        }
                    }
                }
                break;
        }
        return false;
    }
}

